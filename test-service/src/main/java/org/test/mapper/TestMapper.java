package org.test.mapper;

import org.test.entity.Test;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 功能 Mapper 接口
 * </p>
 *
 * @author Funkye
 * @since 2019-04-10
 */
public interface TestMapper extends BaseMapper<Test> {

}